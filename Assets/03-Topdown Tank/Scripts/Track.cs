﻿using UnityEngine;
using System.Collections;

public class Track : MonoBehaviour {

    public float fadeDuration;
    Color trackColor;
    SpriteRenderer spriteRendererComponent;
    float startTime;

    void Awake() {
        // get references to components
        spriteRendererComponent = GetComponent<SpriteRenderer>();
        // set the track tint to white
        trackColor = new Color(1, 1, 1, 1);
        // record the time when instantiated in order to calculate alpha
        startTime = Time.time;
    }

    void Update() {
        Fade();
    }

    void Fade() {

        // calculate the alpha based on time elapsed since instantiation
        float fadePercent = 1 - ((Time.time - startTime) / fadeDuration);

        // set alpha value;
        trackColor.a = fadePercent;

        if (trackColor.a > 0) {
            // assign the new track color
            spriteRendererComponent.color = trackColor;
        } else {
            // destroy the game object if invisible
            Destroy(gameObject);
        }
    }

}
