﻿using UnityEngine;
using System.Collections;

public class Target : MonoBehaviour {

    // create an enum to manage the different states of the target
    public enum State {
        Rising,
        Waiting,
        Falling
    }

    public State state;
    
    public float speed = 3;
    public float waitTime = 2;
    public GameObject bulletHolePrefab;
    public Vector3 offset = new Vector3(0, -3, 0);

    float stopWatchStartTime;
    Vector3 initialPosition;

    void Awake() {
        // record the initial position
        initialPosition = transform.position;
        // translate by the offset
        transform.Translate(offset);
    }

    void Rise() {
        // move up, toward the initial position
        if (transform.position.y < initialPosition.y) {
            transform.Translate(new Vector3(0, speed * Time.deltaTime, 0));
        } else {
            // start timer
            stopWatchStartTime = Time.time;
            // change the state
            state = State.Waiting;
        }
    }

    void Fall() {
        // move down, toward offset
        if (transform.position.y > (initialPosition + offset).y) {
            transform.Translate(new Vector3(0, -speed * Time.deltaTime, 0));
        } else {
            Destroy(gameObject);
        }
    }


    void Wait() {
        // wait for the duration of waitTime before changing states
        if (Time.time - stopWatchStartTime > waitTime) {
            state = State.Falling;
        }
    }

    void OnHit(Vector2 point) {
        // create a bullet hole at the position defined by point (the mouse position)
        GameObject bulletHole = Instantiate(bulletHolePrefab, point, Quaternion.identity) as GameObject;
        // make the new bullet hole game object a child of the target
        bulletHole.transform.parent = transform;
        // force a state change to falling
        state = State.Falling;
    }


    // Update is called once per frame
    void Update() {
        // call the appropriate state code
        switch (state) {
            case State.Rising:
                Rise();
                break;
            case State.Waiting:
                Wait();
                break;
            case State.Falling:
                Fall();
                break;
        }

    }
}
