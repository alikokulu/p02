﻿using UnityEngine;
using UnityEngine.UI;
//hello hello
public class ShootingGalleryManager : MonoBehaviour {

    public Vector2 mouseScreenCoordinates;
    public Vector2 mouseViewPortCoordinates;
    public Vector2 mouseWorldCoordinates;

    public GameObject reticle;

    public int bulletCount;
    int maxBullets = 3;

    public Image[] bulletHUDImages;

    public Sprite bulletHUD;
    public Sprite bulletEmptyHUD;

    public Text scoreText;
    public int score;

    public string scorePrefix;

    string stringFormat = "00#";       // used for formatting the string to pad with leading zeros

    public AudioClip shotAudioClip;
    public AudioClip reloadAudioClip;
    public AudioClip missAudioClip;

    AudioSource audioSourceComponent;

    void Awake() {
        // get references to components
        audioSourceComponent = GetComponent<AudioSource>();
    }

    void Start() {
        Reload();
        UpdateHUD();
    }

    void Shoot() {
        // decrement bullet count
        if (bulletCount > 0) {
            bulletCount--;
            UpdateHUD();
        }
    }

    void ShotHit() {
        // play the gunshot hit sound
        audioSourceComponent.PlayOneShot(shotAudioClip);
    }

    void ShotMiss() {
        // play the gunshot miss sound
        audioSourceComponent.PlayOneShot(missAudioClip);
    }

    void UpdateHUD() {
        // update the HUD text
        scoreText.text = scorePrefix + score.ToString(stringFormat);

        // update the HUD ammo counter
        for (int i = 0; i < maxBullets; i++) {
            // decide whether to show an empty bullet sprite or a regular bullet sprite
            bulletHUDImages[i].sprite = (i < bulletCount) ? bulletHUD : bulletEmptyHUD;

            // same statement as above but written in if / else form
            //
            //if (i < bulletCount) {
            //    bulletInventoryImages[i].sprite = bulletHUD;
            //} else {
            //    bulletInventoryImages[i].sprite = bulletEmptyHUD;
            //}
        }
    }


    void AddToScore() {
        score++;
        UpdateHUD();
    }

    void Reload() {
        bulletCount = maxBullets;
        UpdateHUD();

        // play the reload sound
        audioSourceComponent.PlayOneShot(reloadAudioClip);
    }

    // Update is called once per frame
    void Update() {
        // mouse coordinates in screen, viewport and world spaces
        mouseScreenCoordinates = Input.mousePosition;
        //mouseViewPortCoordinates = Camera.main.ScreenToViewportPoint(mouseScreenCoordinates);
        mouseWorldCoordinates = Camera.main.ScreenToWorldPoint(mouseScreenCoordinates);

        // draw a line to the mouse position in world space
        Debug.DrawLine(Camera.main.transform.position, mouseWorldCoordinates, Color.yellow);
        // set the reticle's position to the mouse position in world space
        reticle.transform.position = mouseWorldCoordinates;

        // right-click to reload
        if (Input.GetMouseButtonDown(1) && bulletCount < maxBullets) {
            Reload();
        }

        // left-click
        if (Input.GetMouseButtonDown(0) && bulletCount > 0) {

            Shoot();

            // get collider with the smallest Z position
            RaycastHit2D hit = Physics2D.Raycast(mouseWorldCoordinates, Vector2.zero);

            if (hit.collider != null) {
                if (hit.collider.tag == "Duck") {
                    // tell the duck that it has been hit
                    hit.collider.transform.parent.SendMessage("OnHit", hit.point);
                    ShotHit();
                    // add to score
                    AddToScore();
                }
            } else {
                ShotMiss();
            }

            // TO GET ALL COLLIDERS THAT ARE HIT BY A RAYCAST, NOT JUST THE CLOSEST COLLIDER, THEN USE Physics2D.RaycastAll()

            //RaycastHit2D[] hits = Physics2D.RaycastAll(mouseWorldCoordinates, Vector2.zero);

            //if (hits.Length > 0) {
            //    for (int i = 0; i < hits.Length; i++) {
            //        Debug.Log("HIT:" + hits[i].collider.name);
            //    }
            //} else {
            //    Debug.Log("MISS!");
            //}

        }

    }
}
