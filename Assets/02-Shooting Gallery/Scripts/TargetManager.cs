﻿using UnityEngine;

public class TargetManager : MonoBehaviour {

    public GameObject targetPrefab;
    public float spawnRangeX;

    public float spawnRate = 2;

    void Start() {
        // create a new target at the spawn rate
        InvokeRepeating("CreateTarget", 0, spawnRate);
    }

    void CreateTarget() {
        // create a new target
        GameObject target = Instantiate(targetPrefab, new Vector3(Random.Range(-spawnRangeX, spawnRangeX), 0, 0), Quaternion.identity) as GameObject;
        // make the new target a child of the target manager
        target.transform.SetParent(transform);
    }

}
