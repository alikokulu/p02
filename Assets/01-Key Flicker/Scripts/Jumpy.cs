﻿using UnityEngine;

public class Jumpy : MonoBehaviour {

    Rigidbody2D rigidbody2DComponent;
    public float maxForce;
    public float maxTorque;

    void Awake() {
        // get references to components
        rigidbody2DComponent = GetComponent<Rigidbody2D>();
    }

    void OnMouseDown() {
        // add force and torque
        rigidbody2DComponent.AddForce(Random.insideUnitCircle * maxForce, ForceMode2D.Impulse);
        rigidbody2DComponent.AddTorque(Random.Range(-1f, 1f) * maxTorque, ForceMode2D.Impulse);
    }
}
