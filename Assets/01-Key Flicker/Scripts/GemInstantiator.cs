﻿using UnityEngine;

public class GemInstantiator : MonoBehaviour {
    public GameObject[] gemPrefabs;
    public int maxGemCount;
    public int gemCount;
    public float interval;

    void Start() {
        // ensure the gem counter is reset to 0
        gemCount = 0;

        // don't create a gem if maxGemCount is not at least 1.
        if (maxGemCount <= 0) return;

        // create gems at a set time interval
        InvokeRepeating("CreateGem", 0f, interval);
    }

    void CreateGem() {
        // select a random gem prefab from the gem prefabs array
        GameObject gemPrefab = gemPrefabs[Random.Range(0, gemPrefabs.Length)];
        // instantiate a new gem
        GameObject gem = Instantiate(gemPrefab, transform.position, Quaternion.identity) as GameObject;
        // assign the newly instantiated gem to be the child of this transform
        gem.transform.parent = transform;
        // add to gem counter
        gemCount++;

           // once the gem counter reaches the max count, stop creating gems
        if (gemCount >= maxGemCount) {
            CancelInvoke();
        }

    }
}
