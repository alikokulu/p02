﻿using UnityEngine;

public class Key : MonoBehaviour {

    Vector3 initialPosition;
    Quaternion initialRotation;

    public float speed;
    public float resetDelay;

    public bool isFlying;

    Rigidbody2D rigidbody2DComponent;
    LineRenderer lineRendererComponent;

    void Awake() {
        // get references to components
        rigidbody2DComponent = GetComponent<Rigidbody2D>();
        lineRendererComponent = GetComponent<LineRenderer>();

        // record the initial position of the key
        initialPosition = transform.position;
        // record the initial rotation of the key
        initialRotation = transform.rotation;

        // set the initial positions of the line renderer points
        lineRendererComponent.SetPosition(0, initialPosition);
        lineRendererComponent.SetPosition(1, initialPosition);

    }

    void OnMouseDown() {
        // if flying, return early -- do nothing
        if (isFlying) return;

        // draw the line
        lineRendererComponent.enabled = true;
    }

    void OnMouseDrag() {
        // if flying, return early -- do nothing
        if (isFlying) return;

        Vector2 mousePositionInWorldSpace = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        // if the key is clicked, have key follow mouse
        transform.position = new Vector3(mousePositionInWorldSpace.x, Mathf.Clamp(mousePositionInWorldSpace.y, -4f, .3f), 0);

        // draw line from origin to the mouse
        lineRendererComponent.SetPosition(1, transform.position);
    }

    void OnMouseUp() {
        // if flying, return early -- do nothing
        if (isFlying) return;

        // stop drawing the line
        lineRendererComponent.enabled = false;

        // get the direction of the key relative to its initial position
        Vector3 directionTowardInitialPosition = initialPosition - transform.position;

        // enable physics on the key
        rigidbody2DComponent.bodyType = RigidbodyType2D.Dynamic;

        // apply an impulse force to the key
        rigidbody2DComponent.AddForce(directionTowardInitialPosition * speed, ForceMode2D.Impulse);

        // prevent the key from being clickable - the key remains unclickable until it is reset.
        isFlying = true;

        // reset the key after a delay
        Invoke("Reset", resetDelay);
    }

    void Reset() {
        // disable physics
        rigidbody2DComponent.bodyType = RigidbodyType2D.Static;
        // reset the key to the initial position
        transform.position = initialPosition;
        // reset the key to the initial rotation
        transform.rotation = initialRotation;
        // allow the key to be clicked once again
        isFlying = false;

        // set the positions of the line renderer points
        lineRendererComponent.SetPosition(0, initialPosition);
        lineRendererComponent.SetPosition(1, initialPosition);
    }

    void OnCollisionEnter2D(Collision2D collision) {
        // destroy the key if it hits the key lock
        if (collision.gameObject.name == "Key Lock") {
            Destroy(gameObject);
        }
    }
}