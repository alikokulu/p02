﻿using UnityEngine;

public class KeyLock : MonoBehaviour {
    public GameObject[] unlockables;
    AudioSource audioSourceComponent;
    Rigidbody2D rigidbody2DComponent;

    void Awake() {
        // get references to components
        audioSourceComponent = GetComponent<AudioSource>();
        rigidbody2DComponent = GetComponent<Rigidbody2D>();
    }

    void OnCollisionEnter2D(Collision2D collision) {
        // if collision with a key
        if (collision.gameObject.name == "Key") {
            foreach (GameObject unlockable in unlockables) {
                unlockable.SendMessage("OnActivate");
            }

            // enable physics on the lock
            rigidbody2DComponent.bodyType = RigidbodyType2D.Dynamic;
            // play unlocking sound
            audioSourceComponent.Play();
            // apply an explosive force
            rigidbody2DComponent.AddForceAtPosition(new Vector2(0, 1500), transform.position);
        }
    }
}
