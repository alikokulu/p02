﻿using UnityEngine;

public class LockTile : MonoBehaviour {

    public float maxVerticalForce;
    Rigidbody2D rigidbody2DComponent;

    void Awake() {
        // get references to components
        rigidbody2DComponent = GetComponent<Rigidbody2D>();
    }

    void OnActivate() {
        // enable physics
        rigidbody2DComponent.bodyType = RigidbodyType2D.Dynamic;
        // apply a random force
        float verticalForce = Random.Range(0, maxVerticalForce);
        rigidbody2DComponent.AddForce(new Vector2(0, 1) * verticalForce, ForceMode2D.Impulse);
    }
}
